#![warn(missing_docs)]
//! This crate contains utility functions for the [Kustomize plugin] of the same name.
//!
//! [Kustomize plugin]: https://gitlab.com/tedtramonte/auto-version-labeler/-/releases

use fancy_regex::{Match, Regex};
use serde::Deserialize;
use serde_yaml::{Mapping, Value};
use thiserror::Error;

/// An error that occurred while transforming a Kustomize `ResourceList`.
#[derive(Debug, Error)]
#[non_exhaustive]
pub enum Error {
    /// An invalid `container` was specified in `.functionConfig.spec`
    /// or more likely in the user's `kustomization.yaml`.
    #[error("specified container not found in resource")]
    InvalidContainer,
    /// An invalid `kind` or `name` was specified in `.functionConfig.spec`
    /// or more likely in the user's `kustomization.yaml`.
    #[error("specified Kind or Name not found in ResourceList")]
    InvalidKindName,
    /// An invalid `style` was specified in `.functionConfig.spec`
    /// or more likely in the user's `kustomization.yaml`.
    #[error("specified style is invalid")]
    InvalidStyle,
    /// The specified `maxLength` in `.functionConfig.spec`
    /// or more likely in the user's `kustomization.yaml` is either too long or too short.
    #[error("specified maxLength is invalid")]
    InvalidMaxLength,
    /// The specified `style` in `.functionConfig.spec`
    /// or more likely in the user's `kustomization.yaml` is not available
    /// for the specified `container`. For example, if `style: sha` is specified
    /// and the specified `container`'s image is `k8s.gcr.io/nginx-slim:0.8`.
    #[error("specified style is not available for the specified container")]
    LabelStyleUnavailable,
    /// An error from the `serde_yaml` crate.
    #[error("error deserializing ResourceList")]
    Yaml(#[from] serde_yaml::Error),
}

/// Extracts a Docker image's tag from the whole `docker_image` name.
///
/// # Panics
/// Panics if `docker_image` isn't `&serde_yaml::Value::String`
/// or if the regular expression has no matches in `docker_image`.
///
/// # Examples
/// ```rust
/// use auto_version_labeler::extract_tag;
///
/// let image = String::from(
///     "mongo:5.0.6@sha256:ad947856db716ddd0b9cc525e341c77208ed8dafcb4a6ad23f9b3addd7a4f71c",
/// );
/// assert_eq!(
///     extract_tag(&serde_yaml::Value::String(image))
///         .unwrap()
///         .as_str(),
///     "5.0.6"
/// );
/// ```
pub fn extract_tag(docker_image: &serde_yaml::Value) -> Option<Match> {
    // RegEx courtesy of Thomas Heuer (https://github.com/tcheuer)
    let re = Regex::new(r"(?<!sha256:)(?<=:)([^:]*)(?=@|$)").unwrap();
    re.find(docker_image.as_str().unwrap()).unwrap()
}

/// Extracts a Docker image's SHA from the whole `docker_image` name.
///
/// # Panics
/// Panics if `docker_image` isn't `&serde_yaml::Value::String`
/// or if the regular expression has no matches in `docker_image`.
///
/// # Examples
/// ```rust
/// use auto_version_labeler::extract_sha;
///
/// let image = String::from(
///     "mongo:5.0.6@sha256:ad947856db716ddd0b9cc525e341c77208ed8dafcb4a6ad23f9b3addd7a4f71c",
/// );
/// assert_eq!(
///     extract_sha(&serde_yaml::Value::String(image))
///         .unwrap()
///         .as_str(),
///     "ad947856db716ddd0b9cc525e341c77208ed8dafcb4a6ad23f9b3addd7a4f71c"
/// );
/// ```
pub fn extract_sha(docker_image: &serde_yaml::Value) -> Option<Match> {
    let re = Regex::new(r"[A-Fa-f0-9]{64}$").unwrap();
    re.find(docker_image.as_str().unwrap()).unwrap()
}

/// Transforms a Kustomize `ResourceList`, adding labels to resources as specified
/// by the `ResourceList`'s `functionConfig` definition.
///
/// # Panics
/// May panic if `.functionConfig.spec` is improperly configured.
///
/// # Examples
/// ```no_run
/// use std::io;
/// use std::io::Read;
///
/// use auto_version_labeler::transform;
///
/// // Read from STDIN, for example
/// let mut input = String::new();
/// let stdin = io::stdin();
/// stdin
///     .lock()
///     .read_to_string(&mut input)
///     .expect("error reading ResourceList");
///
/// let output_resource_list = auto_version_labeler::transform(&input);
/// // Now do whatever with the transformed `ResourceList`
/// // Write to STDOUT, for example
/// ```
pub fn transform(input: &str) -> Result<serde_yaml::Value, Error> {
    let de = serde_yaml::Deserializer::from_str(input);
    let resource_list = Value::deserialize(de)?;
    let mut output_resource_list = resource_list.clone();

    let target_kind = &resource_list["functionConfig"]["spec"]["kind"];
    let target_name = &resource_list["functionConfig"]["spec"]["name"];
    let target_container = &resource_list["functionConfig"]["spec"]["container"];
    let mut target_image = &serde_yaml::Value::Null;
    let label_style = &resource_list["functionConfig"]["spec"]["style"];
    let max_label_length =
        if let Some(length) = &resource_list["functionConfig"]["spec"]["maxLength"].as_u64() {
            if length > &63 {
                return Err(Error::InvalidMaxLength);
            }
            length.to_owned()
        } else {
            63
        };

    if !resource_list["functionConfig"]["spec"]["maxLength"].is_u64()
        && !resource_list["functionConfig"]["spec"]["maxLength"].is_null()
    {
        return Err(Error::InvalidMaxLength);
    }

    let mut valid_kind_name = false;
    let mut valid_container = false;
    for resource in resource_list["items"]
        .as_sequence()
        .expect("error parsing ResourceList items")
    {
        if (resource["kind"].as_str() == target_kind.as_str())
            && (resource["metadata"]["name"].as_str() == target_name.as_str())
        {
            // The specified Resource has been found
            valid_kind_name = true;
            if !target_container.is_null() {
                // A container was specified; Search for that container name
                for (i, container) in resource["spec"]["template"]["spec"]["containers"]
                    .as_sequence()
                    .expect("error parsing containers")
                    .iter()
                    .enumerate()
                {
                    if container["name"].as_str() == target_container.as_str() {
                        valid_container = true;
                        target_image =
                            &resource["spec"]["template"]["spec"]["containers"][i]["image"];
                    }
                }
            } else {
                // No container was specified; Use the first container
                valid_container = true;
                target_image = &resource["spec"]["template"]["spec"]["containers"][0]["image"];
            };

            // Searching complete, skip to processing
            break;
        }
    }
    if !valid_kind_name {
        return Err(Error::InvalidKindName);
    }
    if !valid_container {
        return Err(Error::InvalidContainer);
    }

    let label_match: Option<Match>;
    if let Some(style) = label_style.as_str() {
        match style {
            "tag" => {
                label_match = extract_tag(target_image);
            }
            "sha" => {
                label_match = extract_sha(target_image);
            }
            _ => {
                // Invalid style
                return Err(Error::InvalidStyle);
            }
        }
    } else {
        // Style is unspecified; Default to tag
        label_match = extract_tag(target_image);
    }

    if let Some(value) = label_match {
        let mut label_value: String = value.as_str().to_owned();
        label_value.truncate(max_label_length.try_into().unwrap());

        // Following the commonAnnotations transformer
        // https://github.com/kubernetes-sigs/kustomize/blob/master/examples/transformerconfigs/README.md#annotations-transformer
        for resource in output_resource_list["items"]
            .as_sequence_mut()
            .expect("error parsing ResourceList items")
        {
            // .metadata.labels field for all resources
            resource
                .as_mapping_mut()
                .unwrap()
                .entry("metadata".into())
                .or_insert(Mapping::new().into());
            resource["metadata"]
                .as_mapping_mut()
                .unwrap()
                .entry("labels".into())
                .or_insert(Mapping::new().into());
            resource["metadata"]["labels"]
                .as_mapping_mut()
                .unwrap()
                .entry("app.kubernetes.io/version".into())
                .or_insert(label_value.clone().into());

            // // .spec.template.metadata.labels for specific resources
            if [
                "Deployment",
                "ReplicaSet",
                "DaemonSet",
                "StatefulSet",
                "Job",
            ]
            .contains(&resource["kind"].as_str().unwrap())
            {
                resource
                    .as_mapping_mut()
                    .unwrap()
                    .entry("spec".into())
                    .or_insert(Mapping::new().into());
                resource["spec"]
                    .as_mapping_mut()
                    .unwrap()
                    .entry("template".into())
                    .or_insert(Mapping::new().into());
                resource["spec"]["template"]
                    .as_mapping_mut()
                    .unwrap()
                    .entry("metadata".into())
                    .or_insert(Mapping::new().into());
                resource["spec"]["template"]["metadata"]
                    .as_mapping_mut()
                    .unwrap()
                    .entry("labels".into())
                    .or_insert(Mapping::new().into());
                resource["spec"]["template"]["metadata"]["labels"]
                    .as_mapping_mut()
                    .unwrap()
                    .entry("app.kubernetes.io/version".into())
                    .or_insert(label_value.clone().into());
            }

            // // .spec.jobTemplate.spec.template.metadata.labels for specific resources
            if ["CronJob"].contains(&resource["kind"].as_str().unwrap()) {
                resource
                    .as_mapping_mut()
                    .unwrap()
                    .entry("spec".into())
                    .or_insert(Mapping::new().into());
                resource["spec"]
                    .as_mapping_mut()
                    .unwrap()
                    .entry("jobTemplate".into())
                    .or_insert(Mapping::new().into());
                resource["spec"]["jobTemplate"]
                    .as_mapping_mut()
                    .unwrap()
                    .entry("spec".into())
                    .or_insert(Mapping::new().into());
                resource["spec"]["jobTemplate"]["spec"]
                    .as_mapping_mut()
                    .unwrap()
                    .entry("template".into())
                    .or_insert(Mapping::new().into());
                resource["spec"]["jobTemplate"]["spec"]["template"]
                    .as_mapping_mut()
                    .unwrap()
                    .entry("metadata".into())
                    .or_insert(Mapping::new().into());
                resource["spec"]["jobTemplate"]["spec"]["template"]["metadata"]
                    .as_mapping_mut()
                    .unwrap()
                    .entry("labels".into())
                    .or_insert(Mapping::new().into());
                resource["spec"]["jobTemplate"]["spec"]["template"]["metadata"]["labels"]
                    .as_mapping_mut()
                    .unwrap()
                    .entry("app.kubernetes.io/version".into())
                    .or_insert(label_value.clone().into());
            }
        }
    } else {
        return Err(Error::LabelStyleUnavailable);
    }

    Ok(output_resource_list)
}

#[cfg(test)]
mod extract_tag {
    use super::*;

    #[test]
    fn image_with_semver_tag() {
        let image = String::from(
            "mongo:5.0.6@sha256:ad947856db716ddd0b9cc525e341c77208ed8dafcb4a6ad23f9b3addd7a4f71c",
        );
        assert_eq!(
            extract_tag(&serde_yaml::Value::String(image))
                .unwrap()
                .as_str(),
            "5.0.6"
        );
    }

    #[test]
    fn image_with_no_semver_tag() {
        let image = String::from(
            "mongo@sha256:ad947856db716ddd0b9cc525e341c77208ed8dafcb4a6ad23f9b3addd7a4f71c",
        );
        assert_eq!(extract_tag(&serde_yaml::Value::String(image)), None);
    }

    #[test]
    fn image_with_no_patch_semver_tag() {
        let image = String::from(
            "mongo:5.0@sha256:ad947856db716ddd0b9cc525e341c77208ed8dafcb4a6ad23f9b3addd7a4f71c",
        );
        assert_eq!(
            extract_tag(&serde_yaml::Value::String(image))
                .unwrap()
                .as_str(),
            "5.0"
        );
    }

    #[test]
    fn image_with_no_minor_patch_semver_tag() {
        let image = String::from(
            "mongo:5@sha256:ad947856db716ddd0b9cc525e341c77208ed8dafcb4a6ad23f9b3addd7a4f71c",
        );
        assert_eq!(
            extract_tag(&serde_yaml::Value::String(image))
                .unwrap()
                .as_str(),
            "5"
        );
    }

    #[test]
    fn image_with_non_semver_tag() {
        let image = String::from(
            "mongo:latest@sha256:ad947856db716ddd0b9cc525e341c77208ed8dafcb4a6ad23f9b3addd7a4f71c",
        );
        assert_eq!(
            extract_tag(&serde_yaml::Value::String(image))
                .unwrap()
                .as_str(),
            "latest"
        );
    }

    #[test]
    fn image_with_no_tag() {
        let image = String::from("mongo");
        assert_eq!(extract_tag(&serde_yaml::Value::String(image)), None);
    }

    #[test]
    fn image_registry_with_port_semver_tag() {
        let image = String::from(
            "registry.gitlab.com:5050/tedtramonte/taboobot:1.3.3@sha256:ad947856db716ddd0b9cc525e341c77208ed8dafcb4a6ad23f9b3addd7a4f71c",
        );
        assert_eq!(
            extract_tag(&serde_yaml::Value::String(image))
                .unwrap()
                .as_str(),
            "1.3.3"
        );
    }

    #[test]
    fn image_registry_with_port_non_semver_tag() {
        let image = String::from(
            "registry.gitlab.com:5050/tedtramonte/taboobot:latest@sha256:ad947856db716ddd0b9cc525e341c77208ed8dafcb4a6ad23f9b3addd7a4f71c",
        );
        assert_eq!(
            extract_tag(&serde_yaml::Value::String(image))
                .unwrap()
                .as_str(),
            "latest"
        );
    }
}

#[cfg(test)]
mod extract_sha {
    use super::*;

    #[test]
    fn image_with_sha() {
        let image = String::from(
            "registry.gitlab.com:5050/tedtramonte/taboobot:latest@sha256:ad947856db716ddd0b9cc525e341c77208ed8dafcb4a6ad23f9b3addd7a4f71c",
        );
        assert_eq!(
            extract_sha(&serde_yaml::Value::String(image))
                .unwrap()
                .as_str(),
            "ad947856db716ddd0b9cc525e341c77208ed8dafcb4a6ad23f9b3addd7a4f71c"
        );
    }
    #[test]
    fn image_with_no_sha() {
        let image = String::from("mongo");
        assert_eq!(extract_sha(&serde_yaml::Value::String(image)), None);
    }
}
