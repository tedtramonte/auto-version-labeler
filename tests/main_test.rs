use std::fs::File;
use std::io::prelude::*;

use serde::Deserialize;
use serde_yaml::Value;

// .spec.kind
#[test]
fn resourcelist_spec_with_nonexisting_kind_errors() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/nonexisting-kind/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input);
    assert!(matches!(
        transformed_input,
        Err(auto_version_labeler::Error::InvalidKindName)
    ));
}

// .spec.name
#[test]
fn resourcelist_spec_with_nonexisting_name_errors() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/nonexisting-name/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input);
    assert!(matches!(
        transformed_input,
        Err(auto_version_labeler::Error::InvalidKindName)
    ));
}

// .spec.container
#[test]
fn resourcelist_spec_with_nonexisting_container_errors() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/nonexisting-container/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input);
    assert!(matches!(
        transformed_input,
        Err(auto_version_labeler::Error::InvalidContainer)
    ));
}
#[test]
fn resourcelist_spec_uses_specified_container() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/specific-container/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input).unwrap();

    let mut output = String::new();
    f = File::open("./tests/data/specific-container/output.yaml").unwrap();
    f.read_to_string(&mut output).unwrap();
    let de = serde_yaml::Deserializer::from_str(&output);
    let output_resource_list = Value::deserialize(de).unwrap();

    assert_eq!(*transformed_input, output_resource_list);
}
#[test]
fn resourcelist_spec_without_specified_container_uses_first() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/unspecified-container/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input).unwrap();

    let mut output = String::new();
    f = File::open("./tests/data/unspecified-container/output.yaml").unwrap();
    f.read_to_string(&mut output).unwrap();
    let de = serde_yaml::Deserializer::from_str(&output);
    let output_resource_list = Value::deserialize(de).unwrap();

    assert_eq!(*transformed_input, output_resource_list);
}

// .spec.style
#[test]
fn resourcelist_spec_with_unexpected_style_errors() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/unexpected-style/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input);
    assert!(matches!(
        transformed_input,
        Err(auto_version_labeler::Error::InvalidStyle)
    ));
}
#[test]
fn resourcelist_spec_with_unavailable_style_errors() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/unavailable-style/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input);
    assert!(matches!(
        transformed_input,
        Err(auto_version_labeler::Error::LabelStyleUnavailable)
    ));
}
#[test]
fn resourcelist_spec_with_sha_style_labels_with_sha() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/specific-style/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input).unwrap();

    let mut output = String::new();
    f = File::open("./tests/data/specific-style/output.yaml").unwrap();
    f.read_to_string(&mut output).unwrap();
    let de = serde_yaml::Deserializer::from_str(&output);
    let output_resource_list = Value::deserialize(de).unwrap();

    assert_eq!(*transformed_input, output_resource_list);
}
#[test]
fn resourcelist_spec_without_specified_style_labels_with_tag() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/unspecified-style/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input).unwrap();

    let mut output = String::new();
    f = File::open("./tests/data/unspecified-style/output.yaml").unwrap();
    f.read_to_string(&mut output).unwrap();
    let de = serde_yaml::Deserializer::from_str(&output);
    let output_resource_list = Value::deserialize(de).unwrap();

    assert_eq!(*transformed_input, output_resource_list);
}

// .spec.maxLength
#[test]
fn resourcelist_spec_without_specified_maxlength_is_limited_to_63_chars() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/unspecified-maxlength/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input).unwrap();

    let mut output = String::new();
    f = File::open("./tests/data/unspecified-maxlength/output.yaml").unwrap();
    f.read_to_string(&mut output).unwrap();
    let de = serde_yaml::Deserializer::from_str(&output);
    let output_resource_list = Value::deserialize(de).unwrap();

    assert_eq!(*transformed_input, output_resource_list);
}
#[test]
fn resourcelist_spec_with_maxlength_limits_label_length() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/specific-maxlength/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input).unwrap();

    let mut output = String::new();
    f = File::open("./tests/data/specific-maxlength/output.yaml").unwrap();
    f.read_to_string(&mut output).unwrap();
    let de = serde_yaml::Deserializer::from_str(&output);
    let output_resource_list = Value::deserialize(de).unwrap();

    assert_eq!(*transformed_input, output_resource_list);
}
#[test]
fn resourcelist_spec_with_too_short_maxlength_errors() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/too-short-maxlength/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input);
    assert!(matches!(
        transformed_input,
        Err(auto_version_labeler::Error::InvalidMaxLength)
    ));
}
#[test]
fn resourcelist_spec_with_too_long_maxlength_errors() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/too-long-maxlength/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input);
    assert!(matches!(
        transformed_input,
        Err(auto_version_labeler::Error::InvalidMaxLength)
    ));
}

// Labeling behavior
#[test]
fn resourcelist_items_with_existing_labels_are_preserved_and_merged_with_new_label() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/existing-labels/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input).unwrap();

    let mut output = String::new();
    f = File::open("./tests/data/existing-labels/output.yaml").unwrap();
    f.read_to_string(&mut output).unwrap();
    let de = serde_yaml::Deserializer::from_str(&output);
    let output_resource_list = Value::deserialize(de).unwrap();

    assert_eq!(*transformed_input, output_resource_list);
}
#[test]
fn resourcelist_items_with_no_labels_have_necessary_keys_created_and_only_new_label() {
    let mut input = String::new();
    let mut f = File::open("./tests/data/no-labels/input.yaml").unwrap();
    f.read_to_string(&mut input).unwrap();

    let transformed_input = &auto_version_labeler::transform(&input).unwrap();

    let mut output = String::new();
    f = File::open("./tests/data/no-labels/output.yaml").unwrap();
    f.read_to_string(&mut output).unwrap();
    let de = serde_yaml::Deserializer::from_str(&output);
    let output_resource_list = Value::deserialize(de).unwrap();

    assert_eq!(*transformed_input, output_resource_list);
}
