# Auto Version Labeler
[![License](https://img.shields.io/badge/license-MIT-brightgreen)](https://choosealicense.com/licenses/mit/)
[![GitLab pipeline](https://img.shields.io/gitlab/pipeline-status/tedtramonte/auto-version-labeler)](https://gitlab.com/tedtramonte/auto-version-labeler/-/pipelines)
[![GitLab tag](https://img.shields.io/gitlab/v/tag/tedtramonte/auto-version-labeler?sort=semver)](https://gitlab.com/tedtramonte/auto-version-labeler/-/tags)
[![GitLab release](https://img.shields.io/gitlab/v/release/tedtramonte/auto-version-labeler?sort=semver)](https://gitlab.com/tedtramonte/auto-version-labeler/-/releases)

Auto Version Labeler is a Kustomize plugin (written in Rust) to automatically label Kubernetes resources with a `app.kubernetes.io/version` label matching a specified resource's container image version.

[Kubernetes recommends a set of labels](https://kubernetes.io/docs/concepts/overview/working-with-objects/common-labels/) to make managing clusters easier. I find them quite useful, but the worst part of updating any application is keeping the version metadata accurate, which makes the version label a prime target for automation. Using Kustomize's `Replacement` transformer, I bent the system to my will and had Kustomize automatically label my resources with whatever container image was specified in a related `Deployment` or `Pod`. As I noted in [this Kustomize issue](https://github.com/kubernetes-sigs/kustomize/issues/4555), my neat trick breaks down when the container image is specified using its SHA due to the relatively simplistic nature of the `Replacements` transformer. This plugin aims to "do it right" and offer more flexibility without abusing a Kustomize feature.

## Requirements
- An OCI compliant container engine like Docker or Podman
- Kustomize commands need the `--enable-alpha-plugins` flag
  - See [Usage](#usage) for more information

## Usage
To use this Kustomize plugin, you will need to create a plugin manifest:
```yaml
apiVersion: auto-version-labeler.tedtramonte.com/v1
kind: AutoVersionLabeler
metadata:
  name: auto-version-labeler
  annotations:
    config.kubernetes.io/function: |
      container:
        image: registry.gitlab.com/tedtramonte/auto-version-labeler:latest
spec:
  kind: Deployment # required
  name: taboobot # required
  container: bot # container name, defaults to the first container
  style: tag # tag or sha, defaults to tag
  maxLength: 10 # maximum length of label (1-63), defaults to 63
```

You should include this plugin manifest in the `transformers` field of your `kustomization.yaml` file. The plugin manifest can be referenced by relative path or specified in-line. See [Extending Kustomize](https://kubectl.docs.kubernetes.io/guides/extending_kustomize/) for more information on how to use Kustomize plugins.

Unfortunately, Kustomize plugins are still in Alpha and `kubectl apply -k` does not include the functionality to apply a Kustomize configuration with plugins. However, there are options:
- `kubectl kustomize ./examples/kustomize --enable-alpha-plugins | kubectl apply -f -`
  - This will run run Kustomize on the target configuration and pipe it to kubectl to apply, effectively doing the same thing as `kubectl apply -k`
- Use a Continuous Delivery GitOps tool such as ArgoCD
  - These tools are capable of applying Kustomize configurations, even those using Kustomize plguins (sometimes requires additional configuration)

### Examples
Here is a simple example of a `Deployment` that has its image and labels managed by Kustomize.
```yaml
# plugins/auto-version-labeler.yaml
apiVersion: auto-version-labeler.tedtramonte.com/v1
kind: AutoVersionLabeler
metadata:
  name: auto-version-labeler
  annotations:
    config.kubernetes.io/function: |
      container:
        image: registry.gitlab.com/tedtramonte/auto-version-labeler:latest
spec:
  kind: Deployment
  name: taboobot

# base/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: taboobot
  labels:
    app.kubernetes.io/component: bot
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app.kubernetes.io/component: bot
    spec:
      containers:
        - name: bot
          image: registry.gitlab.com/tedtramonte/taboobot:latest

# base/kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - deployment.yaml
images:
  - name: registry.gitlab.com/tedtramonte/taboobot
    newName: registry.gitlab.com/tedtramonte/taboobot
    newTag: "1.3.3"
commonLabels:
  app.kubernetes.io/name: taboobot
  app.kubernetes.io/part-of: taboobot
transformers:
  - plugins/auto-version-labeler.yaml
```

When Kustomize is run on the above configuration, the output will look something like this:
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app.kubernetes.io/component: bot
    app.kubernetes.io/name: taboobot
    app.kubernetes.io/part-of: taboobot
    app.kubernetes.io/version: 1.3.3
  name: taboobot
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: taboobot
      app.kubernetes.io/part-of: taboobot
  template:
    metadata:
      labels:
        app.kubernetes.io/component: bot
        app.kubernetes.io/name: taboobot
        app.kubernetes.io/part-of: taboobot
        app.kubernetes.io/version: 1.3.3
    spec:
      containers:
      - image: registry.gitlab.com/tedtramonte/taboobot:1.3.3
        imagePullPolicy: Always
        name: bot
```

## Contributing
Merge requests are welcome after opening an issue first.
