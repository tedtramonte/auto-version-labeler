use std::io;
use std::io::Read;

fn main() {
    let mut input = String::new();
    let stdin = io::stdin();
    stdin.lock().read_to_string(&mut input).unwrap();

    match auto_version_labeler::transform(&input) {
        Ok(output_resource_list) => {
            let stdout = io::stdout();
            match serde_yaml::to_writer(stdout, &output_resource_list) {
                Ok(_) => {
                    std::process::exit(0);
                }
                Err(err) => {
                    eprintln!("error: auto-version-labeler: {}", err);
                    std::process::exit(1);
                }
            }
        }
        Err(err) => {
            eprintln!("error: auto-version-labeler: {}", err);
            std::process::exit(1);
        }
    }
}
