FROM rust:1.70.0@sha256:508253a23bfe549d8afb3027198e1882c2b09fd34898a83163a23f395f410b11 as builder
ARG CARGO_MAKE_PROFILE="production"
WORKDIR /build
RUN apt update && apt install -y musl-tools musl-dev
RUN cargo install --force cargo-make
COPY ./Cargo.toml Cargo.toml
COPY ./Cargo.lock Cargo.lock
COPY ./Makefile.toml Makefile.toml
COPY ./src ./src
RUN cargo make --profile ${CARGO_MAKE_PROFILE} build


FROM scratch

# Copy standard users over, specifically nobody
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

# Install auto-version-labeler
COPY --from=builder /build/dist/auto-version-labeler /usr/local/bin/auto-version-labeler

# Drop root
USER nobody

ENTRYPOINT ["auto-version-labeler"]
